package GraphicCore

import (
    "fmt"
    "strconv"
)

type GraphicCore struct {
  width int
  heigth int
  prevFrameBuffer [][]byte
  NextFrameBuffer [][]byte
}

func (gc * GraphicCore) Init(width int, heigth int) {
  gc.width = width
  gc.heigth = heigth
  gc.prevFrameBuffer = make([][]byte, width)
  gc.NextFrameBuffer = make([][]byte, width)
  fmt.Println("\033[2J");
  for x := 0; x < width; x++ {
    gc.prevFrameBuffer[x] = make([]byte, heigth)
    gc.NextFrameBuffer[x] = make([]byte, heigth)
    for y := 0; y < heigth; y++ {
      gc.NextFrameBuffer[x][y] = ' '
      gc.prevFrameBuffer[x][y] = ' '
      fmt.Println("\033[" + strconv.Itoa(gc.heigth - y) + ";" + strconv.Itoa(x) + "H" + string(gc.NextFrameBuffer[x][y]))
    }
  }
}

func (gc * GraphicCore) Finish() {
  for x := 0; x < gc.width; x++ {
    for y := 0; y < gc.heigth; y++ {
      fmt.Println("\033[" + strconv.Itoa(gc.heigth - y) + ";" + strconv.Itoa(x) + "H ")
    }
  }
  fmt.Print("\033[" + "0" + ";" + "0" + "H")
}

func (gc * GraphicCore) Draw() {
  for x:= 0; x < gc.width; x++ {
    for y:= 0; y < gc.heigth; y++ {
      if gc.NextFrameBuffer[x][y] != gc.prevFrameBuffer[x][y]{
        fmt.Println("\033[" + strconv.Itoa(gc.heigth - y) + ";" + strconv.Itoa(x+1) + "H" + string(gc.NextFrameBuffer[x][y]))
        gc.prevFrameBuffer[x][y] = gc.NextFrameBuffer[x][y]
      }
    }
  }
}

func (gc * GraphicCore) GetInput(){
  //ToDo
}

func (gc * GraphicCore) Clear() {
  for x := 0; x < gc.width; x++ {
    for y := 0; y < gc.heigth; y++ {
      gc.NextFrameBuffer[x][y] = ' '
    }
  }
}

func (gc * GraphicCore) RenderSprite(x int, y int, sprite string) {
  lines := []string{ }
  sprite = "\n" + sprite
  for _ , symbol := range sprite {
    if (symbol == '\n') {
      lines = append(lines, "")
    } else {
      lines[len(lines) - 1] += string(symbol)
    }
  }
  for dy := 0; dy < len(lines); dy ++{
    if y + dy >= gc.heigth { break }
    dx := 0
    for _ , symbol := range lines[len(lines) - 1 - dy]{
      if x + dx >= gc.width { break }
      gc.NextFrameBuffer[x + dx][gc.heigth - 1 - (y + dy)] = byte(symbol)
      dx ++
    }
  }
}

func (gc * GraphicCore) RenderText(x int, y int, text string) {
  dx := 0
  for _ , symbol := range text{
    if x + dx >= gc.width { break }
    gc.NextFrameBuffer[x + dx][y] = byte(symbol)
    dx ++
  }
}

func (gc * GraphicCore) ReadInput() string {
  fmt.Println("\033[" + strconv.Itoa(gc.heigth - 1) + ";" + "1" + "H")
  var s string
  fmt.Scanln(&s)
  for x:= 0; x < gc.width; x++ {
    gc.prevFrameBuffer[x][0] = ')'
  }
  gc.Draw()
  return s
}
