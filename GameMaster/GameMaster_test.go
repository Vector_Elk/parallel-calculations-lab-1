package GameMaster

import (
	"testing"
	GraphicCore "../GraphicCore"
)

func TestGameOver(t *testing.T){
	var graphicCore GraphicCore.GraphicCore

	var gameMaster GameMaster
  gameMaster.Init(&graphicCore, true)

  if gameMaster.CheckTopEmpty(){
		t.Error("CheckTopEmpty false positive")
  }

  if gameMaster.CheckBotEmpty(){
		t.Error("CheckBotEmpty false positive")
  }

  if gameMaster.CheckGameOver(){
		t.Error("Game is over, but it shouldn't")
  }
}

func TestTopEmpty(t *testing.T){
	var graphicCore GraphicCore.GraphicCore

	var gameMaster GameMaster
  gameMaster.Init(&graphicCore, true)

	for i:=0; i < 6; i++{
		gameMaster.boxes[i] = 0
	}

	for i:=7; i < 13; i++{
		gameMaster.boxes[i] = 1
  }

  if !gameMaster.CheckGameOver(){
    t.Error("Game should be over, but it is not")
  }

	if !gameMaster.CheckTopEmpty(){
		t.Error("CheckTopEmpty does not work")
  }

  if gameMaster.CheckBotEmpty(){
		t.Error("CheckBotEmpty false positive")
  }
}

func TestBotEmpty(t *testing.T){
	var graphicCore GraphicCore.GraphicCore

	var gameMaster GameMaster
  gameMaster.Init(&graphicCore, true)

  for i:=0; i < 6; i++{
		gameMaster.boxes[i] = 1
  }

  for i:=7; i < 13; i++{
		gameMaster.boxes[i] = 0
  }

  if !gameMaster.CheckGameOver(){
		t.Error("Game should be over, but it is not")
  }

  if !gameMaster.CheckBotEmpty(){
		t.Error("CheckBotEmpty does not work")
  }

  if gameMaster.CheckTopEmpty(){
		t.Error("CheckTopEmpty false positive")
  }
}

func TestCollect(t *testing.T){
  var graphicCore GraphicCore.GraphicCore

  var gameMaster GameMaster
  gameMaster.Init(&graphicCore, true)

  gameMaster.Collect()

  if !gameMaster.CheckGameOver(){
    t.Error("Should be empty but its not")
  }

  if !gameMaster.CheckTopEmpty(){
		t.Error("Top not empty, but it should be")
  }

  if !gameMaster.CheckBotEmpty(){
		t.Error("Bot not empty, but it should be")
  }
}
