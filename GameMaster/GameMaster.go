package GameMaster

import (
    GraphicCore "../GraphicCore"
    "strconv"
    "time"
)

type GameMaster struct{
  boxes [14]int
  graphicCore * GraphicCore.GraphicCore
  turnP1 bool
  selectedBox int
}

func (gm * GameMaster) Init(graphicCore * GraphicCore.GraphicCore, turnP1 bool) {
  gm.graphicCore = graphicCore
  gm.turnP1 = turnP1
  for i := 0; i < 6;  i++ { gm.boxes[i] = 4 }
  for i := 7; i < 13; i++ { gm.boxes[i] = 4 }
  gm.selectedBox = -1
}

func (gm * GameMaster) CheckTopEmpty() bool {
  for i := 0; i < 6; i++ {
    if gm.boxes[i] != 0 {
      return false
    }
  }
  return true
}

func (gm * GameMaster) CheckBotEmpty() bool {
  for i := 7; i < 13; i++ {
    if gm.boxes[i] != 0 {
      return false
    }
  }
  return true
}

func (gm * GameMaster) CheckGameOver() bool {
  if (gm.CheckBotEmpty() || gm.CheckTopEmpty()){
    return true
  }
  return false
}

func (gm * GameMaster) Collect() {
  var leftover int = 0
  for i:= 0; i < 6; i++ {
    leftover += gm.boxes[i]
    gm.boxes[i] = 0
  }
  gm.boxes[6] += leftover
  leftover = 0
  for i:= 7; i < 13; i++ {
    leftover += gm.boxes[i]
    gm.boxes[i] = 0
  }
  gm.boxes[13] += leftover
}

func boxContentToText(content int) string{
  s := strconv.Itoa(content)
  if content < 10 { s += " " }
  return s
}

func (gm * GameMaster) renderBoxesContent() {
  for i := 0; i < 6; i++ {
    gm.graphicCore.RenderText(11 * i + 12,4,boxContentToText(gm.boxes[i]))
  }
  for i := 7; i < 13; i++ {
    gm.graphicCore.RenderText(11 * (12 - i) + 12,14,boxContentToText(gm.boxes[i]))
  }
  gm.graphicCore.RenderText(5,9,boxContentToText(gm.boxes[13]))
  gm.graphicCore.RenderText(74,9,boxContentToText(gm.boxes[6]))
}

func (gm * GameMaster) cycleBoxes() {
  if(gm.selectedBox == 13 && gm.turnP1){
    gm.selectedBox ++
  }
  if(gm.selectedBox == 6 && !gm.turnP1){
    gm.selectedBox ++
  }
  if(gm.selectedBox < 0) {
    gm.selectedBox = 14 - ((-gm.selectedBox) % 14)
  } else {
    gm.selectedBox = gm.selectedBox % 14
  }
}

func (gm * GameMaster) clearSelector(num int) {
  if num < 3 {
    gm.graphicCore.RenderText(num * 11 + 12, 6, " ")
  }
  if (num >= 3) && (num < 6) {
    gm.graphicCore.RenderText(num * 11 + 13, 6, " ")
  }
  if (num >= 7) && (num < 10) {
    gm.graphicCore.RenderText((12 - num) * 11 + 12, 12, " ")
  }
  if (num >= 10) && (num < 13) {
    gm.graphicCore.RenderText((12 - num) * 11 + 13, 12, " ")
  }
  if num == 6 {
    gm.graphicCore.RenderText(74, 7, " ")
  }
  if num == 13 {
    gm.graphicCore.RenderText(6, 7, " ")
  }
}

func (gm * GameMaster) renderSelector() {
  for i := 0; i < 14; i++ { gm.clearSelector(i) }

  if gm.selectedBox == -1 { return }

  gm.cycleBoxes()

  if gm.selectedBox < 3 {
    gm.graphicCore.RenderText(gm.selectedBox * 11 + 12, 6, "*")
  }
  if (gm.selectedBox >= 3) && (gm.selectedBox < 6) {
    gm.graphicCore.RenderText(gm.selectedBox * 11 + 13, 6, "*")
  }
  if (gm.selectedBox >= 7) && (gm.selectedBox < 10) {
    gm.graphicCore.RenderText((12 - gm.selectedBox) * 11 + 12, 12, "*")
  }
  if (gm.selectedBox >= 10) && (gm.selectedBox < 13) {
    gm.graphicCore.RenderText((12 - gm.selectedBox) * 11 + 13, 12, "*")
  }
  if gm.selectedBox == 6 {
    gm.graphicCore.RenderText(74, 7, "*")
  }
  if gm.selectedBox == 13 {
    gm.graphicCore.RenderText(6, 7, "*")
  }
}

func (gm * GameMaster) renderTurnPointer() {
  if gm.turnP1 {
    gm.graphicCore.RenderText(40,9,"bottom")
  } else {
    gm.graphicCore.RenderText(40,9,"top   ")
  }
}

func (gm * GameMaster) renderBoard() {
  //game field
  board := `
          1          2          3         4          5          6
        ----       ----       ----       ----       ----       ----
       | 0  |     | 0  |     | 0  |     | 0  |     | 0  |     | 0  |
        ----       ----       ----       ----       ----       ----


 ----                                                                 ----
| 0  |                         turn: bottom                          | 0  |
 ----                                                                 ----


        ----       ----       ----       ----       ----       ----
       | 0  |     | 0  |     | 0  |     | 0  |     | 0  |     | 0  |
        ----       ----       ----       ----       ----       ----
          1          2          3         4          5          6


  `
  gm.graphicCore.RenderSprite(3,0,board)
}

func (gm * GameMaster) drawFrame() {
  gm.renderBoard()
  gm.renderBoxesContent()
  gm.renderSelector()
  gm.renderTurnPointer()
  gm.graphicCore.Draw()
}

func (gm * GameMaster) animateTurn() {
  gm.cycleBoxes()
  savedSelection := gm.selectedBox
  beansLeft := gm.boxes[gm.selectedBox]
  time.Sleep(time.Millisecond * 200)
  gm.boxes[gm.selectedBox] = 0
  gm.drawFrame()
  for beansLeft > 0 {
    time.Sleep(time.Millisecond * 590)
    gm.selectedBox ++
    gm.cycleBoxes()
    gm.boxes[gm.selectedBox] ++
    beansLeft --
    gm.drawFrame()
  }
  if (gm.selectedBox == 6) || gm.selectedBox == 13 {
    gm.turnP1 = !gm.turnP1
  } else {
    if gm.boxes[gm.selectedBox] == 1 && !gm.turnP1 && gm.selectedBox > 6{
      gm.animateGrab()
    }
    if gm.boxes[gm.selectedBox] == 1 && gm.turnP1 && gm.selectedBox < 6{
      gm.animateGrab()
    }
  }

  gm.selectedBox = savedSelection
  gm.drawFrame()
}

func (gm * GameMaster) animateGrab() {
  time.Sleep(time.Millisecond * 590)
  var opposingBox int
  opposingBox = 12 - gm.selectedBox
  sum := gm.boxes[gm.selectedBox]
  sum += gm.boxes[opposingBox]
  gm.boxes[gm.selectedBox] = 0
  gm.boxes[opposingBox] = 0
  var turnbox int
  if gm.turnP1 {
    turnbox = 6
  } else {
    turnbox = 13
  }
  gm.boxes[turnbox] += sum

}

func (gm * GameMaster) getRealPotNumber(potnumber int) int {
  if gm.turnP1 {
    return potnumber - 1
  }
  return 13 - potnumber
}

func (gm * GameMaster) readPotNumber() int {
  var potnumber int = 0
  for true {
    potnumber, _ = strconv.Atoi(gm.graphicCore.ReadInput())
    if potnumber > 6 { continue }
    if potnumber < 1 { continue }
    potnumber = gm.getRealPotNumber(potnumber)
    if gm.boxes[potnumber] == 0 {continue}
    break
  }
  return potnumber
}

func (gm * GameMaster) win(p1 bool) {
  gm.graphicCore.Clear()
  if p1 {
    gm.graphicCore.RenderText(32,9, "bottom player wins!")
  } else {
    gm.graphicCore.RenderText(32,9, "top player wins")
  }
  gm.graphicCore.Draw()
  gm.graphicCore.ReadInput()
  gm.graphicCore.Finish()
}

func (gm * GameMaster) Play() {
  for true {
    gm.drawFrame()
    gm.selectedBox = gm.readPotNumber()
    gm.animateTurn()
    gm.turnP1 = !gm.turnP1
    gm.selectedBox = -1
    if(gm.CheckGameOver()){
      gm.Collect()
      if gm.boxes[6] > gm.boxes[13] {
        gm.win(true)
      } else {
        gm.win(false)
      }
      return
    }
  }
}
