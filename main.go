package main

import (
    "./GraphicCore"
    "./GameMaster"
)

func main() {
    var graphicCore GraphicCore.GraphicCore
    graphicCore.Init(80,20)

    var gameMaster GameMaster.GameMaster
    gameMaster.Init(&graphicCore, true)

    gameMaster.Play()
}
